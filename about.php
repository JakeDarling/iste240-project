<!DOCTYPE html>
<html>
  <!-- Homepage for Group 10 ISTE240 project -->
  <head>
    <!-- Each page should open the html and head tag, and provide a title -->
    <title>Consignment Shop</title>
    <!-- Include the header, which includes bootstrap and closes the head tag, opens the body tag -->
    <?php
    include 'header.php';
    ?>
    <!-- Include the navigation bar, uses nav tags-->
    <?php
    include 'navbar.php';
    ?>
    <!-- Main content for this page -->
    <div class="container">
		 <div class="row">
			<p>Welcome to the Consignment Shop!</p>
		</div>
		
		<!-- Hours/Contact/Map -->
		<div class="row">
		<div class="col-sm-4">
		  <h3>Hours</h3>
		  <p>
			Sunday 10am-6pm <br>
			Monday 9am-9pm <br>
			Tuesday 9am-9pm <br>
			Wednesday 9am-9pm <br>
			Thursday 9am-9pm <br>
			Friday 9am-11pm <br>
			Saturday 9am-11pm <br>
		  </p>
		</div>
		<div class="col-sm-4">
		   <h3>Contact Us</h3>
		  <p>
			Phone number: 123-456-7890 <br>
			Email: consignment@shop.com <br>
			Facebook: facebook.com/consignmentshop
		  </p>
		</div>
		<div class="col-sm-4">
		  <h3>Address</h3>
		  <p>
			1 South Wedge Dr, Rochester NY 14623
		  </p>
		</div>
	  </div>
	  <br><br>
	  
    </div>
    <!-- Include the footer, uses footer tags, closes the body tag -->
    <?php
    include 'footer.php';
    ?>
</html>