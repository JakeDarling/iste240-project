<!DOCTYPE html>
<html>
<!-- Listing page for any kind of clothing -->
<head>
<!-- Each page should open the html and head tag, and provide a title -->
<?php
echo '<title>Become a Consignor</title>';

include 'header.php'; 
include 'navbar.php';

// Start container for width, heading, and well for style
echo '
<div class="container">
';
echo '<div class="well">'; 

echo '<p>Please fill out the form below if you have an item you wish to consign for. 
We will get back to you as soon as possible. If we are interested in selling your item, we will email you
and arrange a time to bring the item to the store to assess its quality and take photographs for the site.</p>';

echo '
<form name="consignorContact" action="" class="consignor-form">
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email"> 
    </div>
    <div class= "form-group">
        <label for="price">Ideal Price:</label>
        <input type="text" class="form-control" id="price"> 
    </div>
    <div class= "form-group">
        <label>Item Name/Description:</label>
        <textarea rows="10" cols="100" class="form-control" id="description"></textarea>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>

</form>
';


echo '
    </div>
</div> <!-- End container and well -->
';
include 'footer.php';
?>
