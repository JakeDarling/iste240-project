<!DOCTYPE html>
<html>
  <!-- Homepage for Group 10 ISTE240 project -->
  <head>
    <!-- Each page should open the html and head tag, and provide a title -->
    <title>Consignment Shop</title>
    <!-- Include the header, which includes bootstrap and closes the head tag, opens the body tag -->
    <?php
    include 'header.php';
    ?>
    <!-- Include the navigation bar, uses nav tags-->
    <?php
    include 'navbar.php';
    ?>
    <!-- Main content for this page -->
    <div class="container">
		<!--
			code borrowed from W3schools
			http://www.w3schools.com/bootstrap/bootstrap_carousel.asp
		-->
		<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
		  </ol>
		  

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
			<div class="item active">
			  <img src="assets/images/carousel_images/SW.jpg" alt="Image1">
			</div>

			<div class="item">
			  <img src="assets/images/carousel_images/rack.jpg" alt="Image1">
			</div>

			<div class="item">
			  <img src="assets/images/carousel_images/fashiontrend.jpg" alt="Image1">
			</div>

			<div class="item">
			  <img src="assets/images/carousel_images/shopping.jpg" alt="Image1">
			</div>
		  </div>

		  <!-- Left and right controls 
		  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		  </a> 
		  -->
		</div>
		<br><br>
   <div class="row">
		<h4>Welcome to the Consignment Shop! We are located in the beautiful South Wedge
			area. Come and check out our fantastic discounted prices or become a consignor!
			</h4>
	</div>	
    <!-- Include the footer, uses footer tags, closes the body tag -->
    <?php
    include 'footer.php';
    ?>
</html>

<!-- picture sources - putting them here for now

South Wedge - http://www.newdigs.com/blog/wp-content/uploads/2010/08/august-2010-download-007-e1280775228540.jpg
Model - http://s2.favim.com/orig/32/clothes-fashion-model-shoes-skirt-Favim.com-253553.jpg
Shopping - http://sourcethestation.com/wp-content/uploads/2012/08/consignment-shopping.jpg
Rack - http://www.treesfullofmoney.com/wp-content/uploads/2010/01/consignment-shop.jpg

-->