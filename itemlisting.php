<!DOCTYPE html>
<html>
<!-- Listing page for any kind of clothing -->
<head>
<!-- Each page should open the html and head tag, and provide a title -->
<?php
// Number of listing items per row
// note that changing this may be bad as the bootstrap is not dynamically calculated 
$ITEMS_PER_ROW = 4;
// Title text to display for each page
$type_as_text = array(
    "dress" => "Dresses",
    "top" => "Tops",
    "sweater" => "Sweaters",
    "denim" => "Denim",
    "pants" => "Pants",
    "legs" => "Leggings",
    "shorts" => "Shorts",
    "skirt" => "Skirts",
    "active" => "Activewear",
    "sleep" => "Sleepwear",
    "swim" => "Swimwear",
    "jacket" => "Jackets/Coats",
    "jewelry" => "Jewelry",
    "watch" => "Watches",
    "beauty" => "Beauty",
    "shoes" => "Shoes",
    "bag" => "Bags",
    "wallet" => "Wallets",
    "hairacc" => "Hair Accessories",
    "hat" => "Hats",
    "scarves" => "Scarves",
    "glove" => "Gloves",
);
// The string we will use as the 'type' to get from the db,
// passed in the url like itemlisting.php?type=dress
$type = $_GET['type'];

// If type is not supplied, default to tops
if (empty($type)) {
    $type = "top";
}

echo '<title>Shop '. $type_as_text[$type] .' at Sarah\'s</title>';

include 'header.php'; 
include 'navbar.php';

// Start container for width, heading, and well for style
echo '
<div class="container">
';
echo '<h2>'. $type_as_text[$type] .'</h2>';
echo '<div class="well">'; 


//                                 hardcoding credentials is bad
$mysqli = new mysqli("localhost", "group10", "xeg3511", "group10");

/* check connection */
if (mysqli_connect_errno()) {
    echo "Connect failed: %s\n" . mysqli_connect_error();
    exit();
}

/*
 * This is a 'prepared statement'. It makes passing parameters to SQL queries easier and safer.
 * Instead of using string concatenation like "WHERE type = (" . $type . ");" you simply use a ?
 * and then replace it later with a variable in the 'bind' step.
 *
 * Prapared statements are also the only surefire way to protect against SQL injection (wikipedia explains this well)
 */
if ($stmt = $mysqli->prepare("SELECT cost, name, imgPath FROM clothing WHERE type = (?);")) {
    
    // This replaces the ? in the statement with $type
    $stmt->bind_param("s", $type); // first argument is type of parameter. s: string, i: int, etc

    // Execute the query on the db
    $stmt->execute();

    // Get the results of the query, store them into 3 variables since we selected 3 things
    $stmt->bind_result($rCost, $rName, $rImgPath); // This is nicer in a new version of php

    $i = 0;
    // Output image, cost, and name of each item
    while($stmt->fetch()) { // NOTE: This is different than when using non-prepared statements
        if ($i % $ITEMS_PER_ROW == 0) { // Start new row
            echo '<div class="row">';
        } 
        // Display a row entry
        echo '<div class="col-sm-2 listing-item">';
        echo sprintf('<img class="listing-img" src="%s" alt="%s" title="%s">', "assets/images/" . $rImgPath, $rName, $rName);
        echo sprintf('<p class="listing-title">%s</p>', $rName);
        echo sprintf('<p class="listing-price">$%s</p>', money_format('%(#6n', $rCost));
        echo '</div>';

        $i++;
        if ($i % $ITEMS_PER_ROW == 0) { // Close row
            echo '</div>'; 
        }
        
    }
    // No results were found
    if (empty($rCost)) { //                        XSS protection
        echo '<div>No items were found of type: '. htmlentities($type, ENT_QUOTES, "UTF-8") .'</div>';
    }
    
    // Close statement so we don't leak resources
    // Doesn't really matter since php terminates in like 14 lines anyway
    $stmt->close();
} else {
    echo "Prepared statement creation failed! - Contact site administrator";
}

// Close db connection 
$mysqli->close();

echo '
    </div>
</div> <!-- End container and well -->
';
include 'footer.php';
?>
