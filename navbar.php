<?php
//TODO navbar

echo '
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/~group10/project/index.php">Consignment Shop</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="/~group10/project/about.php">About</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Clothing <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/~group10/project/itemlisting.php?type=dress">Dresses</a></li>
            <li><a href="/~group10/project/itemlisting.php?type=top">Tops</a></li>
            <li><a href="/~group10/project/itemlisting.php?type=sweater">Sweaters</a></li>
            <li class="divider"></li>
      <li><a href="/~group10/project/itemlisting.php?type=denim">Denim</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=pants">Pants</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=legs">Leggings</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=shorts">Shorts</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=skirt">Skirts</a></li>
            <li class="divider"></li>
      <li><a href="/~group10/project/itemlisting.php?type=active">Activewear</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=sleep">Sleepwear</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=swim">Swimwear</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=jacket">Jackets/Coats</a></li>
          </ul>
        </li>
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Accessories <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/~group10/project/itemlisting.php?type=jewelry">Jewelry</a></li>
            <li><a href="/~group10/project/itemlisting.php?type=watch">Watches</a></li>
            <li><a href="/~group10/project/itemlisting.php?type=beauty">Beauty</a></li>
            <li class="divider"></li>
            <li><a href="/~group10/project/itemlisting.php?type=shoes">Shoes</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=bag">Bags</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=wallet">Wallets</a></li>
            <li class="divider"></li>
      <li><a href="/~group10/project/itemlisting.php?type=hairacc">Hair Accessories</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=hat">Hats</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=scarves">Scarves</a></li>
			<li><a href="/~group10/project/itemlisting.php?type=glove">Gloves</a></li>
          </ul>
        </li>
      </ul>
	  <ul class="nav navbar-nav">
		<li><a href="/~group10/project/southwedge.php">South Wedge</a></li>
		<li><a href="/~group10/project/consignor.php">Consignor</a></li>
	  </ul>
    </div>
  </div>
</nav>
'

?>