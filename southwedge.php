<!DOCTYPE html>
<html>
  <!-- Homepage for Group 10 ISTE240 project -->
  <head>
    <!-- Each page should open the html and head tag, and provide a title -->
    <title>Consignment Shop</title>
    <!-- Include the header, which includes bootstrap and closes the head tag, opens the body tag -->
    <?php
    include 'header.php';
    ?>

    <!-- Include the navigation bar, uses nav tags-->
    <?php
    include 'navbar.php';
    ?>
    <!-- Main content for this page -->
    <div class="container">

		<!--
			code borrowed from W3schools
			http://www.w3schools.com/bootstrap/bootstrap_carousel.asp
		-->
		<br><br>
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
			<div class="item active">
			  <img src="assets/images/sw1.jpg" alt="Image1">
			  <div class="carousel-caption">
				<h3>South Wedge Town Square</h3>
				<p></p>
			  </div>
			</div>

			<div class="item">
			  <img src="assets/images/sw2.jpg" alt="Image1">
			  <div class="carousel-caption">
				<h3>South Wedge Diner</h3>
				<p></p>
			  </div>
			</div>

			<div class="item">
			  <img src="assets/images/sw3.jpg" alt="Image1">
			  <div class="carousel-caption">
				<h3>Rochester Real Beer Expo</h3>
				<p></p>
			  </div>
			</div>

			<div class="item">
			  <img src="assets/images/sw4.jpg" alt="Image1">
			  <div class="carousel-caption">
				<h3>The South Wedge Historic District</h3>
				<p></p>
			  </div>
			</div>
		  </div>

		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		  </a>
		</div>
		<br><br>

		<!-- histroy -->
		<div class="row">
			<!-- <div class="col-sm-4"> -->
				<h3>A proud, historic community</h3>
				<p>The South Wedge neighborhood, started in the 1820s, has had its ups and downs.  In its infancy, it was just a few homes owned by people involved in trading on the Erie canal, but by the 1860s, it had begun to prosper, with a hospital, several commercial buildings, a plank road, and even the house of Frederick Douglass, a famous abolitionist.
				</p>
    			<p>But after World War II, the construction of suburbs drew people out of the South Wedge area.  Businesses closed and homes were left behind until the neighborhood was overrun with crime and poverty in the 1970s.  Residents took matters into their own hands, however, forming the South Wedge Planning Committee in 1973.  From there, the South Wedge area was set on a slow, steady path towards recovery.
    			</p>
    			<p>Now, the South Wedge is home to all sorts of establishments.  The sidewalks are clustered with cafes, live entertainment venues, restaurants, shops, and even more.  It might be easier for you to visit than for us to describe it all!
				</p>
			<!-- </div> -->

		</div>
		
		<!-- events -->
		<div class="row">
		<div class="col-sm-4">
		  <h3>Event 1</h3>
		  <img src="assets/images/lilacfest.jpg" alt="Image1">
		  <p>Rochester Lilac Festival</p>
		  <p><a href="http://www.rochesterevents.com/lilac-festival/">Lilac Festival Webpage</a></p>
		</div>
		<div class="col-sm-4">
		   <h3>Event 2</h3>
		  <img src="assets/images/parkavefest.jpg" alt="Image1">
		  <p>Park Ave Summer Art Fest</p>
		  <p><a href="http://www.rochesterevents.com/park-ave-festival/">Park Ave Fest Webpage</a></p>
		</div>
		<div class="col-sm-4">
		  <h3>Event 3</h3>
		  <img src="assets/images/harvesthoot.jpg" alt="Image1">
		  <p>Harvest Hootenanny</p>
		  <p><a href="http://www.southwedge.com/special-event/harvest-hootenanny/">Harvest Hootenanny Webpage</a></p>
		</div>
	  </div>
	  <br><br>
  
    </div>
    <!-- Include the footer, uses footer tags, closes the body tag -->
    <?php
    include 'footer.php';
    ?>
</html>